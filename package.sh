#!/bin/sh

set -e

PKGNAME=freebsd92
PKGFILES=.

TSTAMP=`date "+%Y%m%d%H%M%S"`

tar --exclude './.git' -cvyf /tmp/${PKGNAME}-sippy-${TSTAMP}.tbz2 ${PKGFILES}
git tag rel.${TSTAMP}
git push origin rel.${TSTAMP}
scp /tmp/${PKGNAME}-sippy-${TSTAMP}.tbz2 sobomax@download.sippysoft.com:/usr/local/www/data/freebsd/
