/*-
 * Copyright (c) 1999 Assar Westerlund
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $FreeBSD: base/stable/9/share/examples/kld/syscall/test/call.c 209199 2010-06-15 09:30:36Z kib $
 */


#include <sys/types.h>
#include <sys/module.h>
#include <sys/syscall.h>

#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "../module/syscall.h"

int
main(int argc __unused, char **argv __unused)
{
	int syscall_num, i, modid;
	struct module_stat stat;
        struct sendto_s ss_send[10];
        struct recvfrom_s ss_recv[10];

	stat.version = sizeof(stat);
	modid = modfind("net_bulk");
	if (modid < 0) {
		err(1, "modfind(net_bulk)");
	}
	if (modstat(modid, &stat) != 0) {
		err(1, "modstat(net_bulk)");
	}
	syscall_num = stat.data.intval;
        memset(ss_send, 0x1, sizeof(ss_send));
        memset(ss_recv, 0x2, sizeof(ss_recv));
        BULK_NET_INIT_SS(ss_send, 10);
        BULK_NET_INIT_SS(ss_recv, 10);
	syscall(syscall_num, ss_send, 10);
	for (i = 0; i < 10; i++) {
		printf("ss[%d].rval = %d\n", i, (int)ss_send[i].rval);
	}
        syscall(syscall_num + 1, ss_recv, 10);
        for (i = 0; i < 10; i++) {
                printf("ss_recv[%d].rval = %d\n", i, (int)ss_recv[i].rval);
        }

	return (0);
}
