/* $OpenBSD: version.h,v 1.66 2013/02/10 21:19:34 markus Exp $ */
/* $FreeBSD: stable/9/crypto/openssh/version.h 251135 2013-05-30 12:25:58Z des $ */

#define SSH_VERSION	"OpenSSH_6.2"

#define SSH_PORTABLE	"p2"
#define SSH_RELEASE	SSH_VERSION SSH_PORTABLE

#define SSH_VERSION_FREEBSD	"FreeBSD-20130515"
#define SSH_VERSION_HPN		"_hpn13v11"
